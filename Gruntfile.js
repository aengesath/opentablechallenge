module.exports = function(grunt) {

    var cssFiles = [
        'public/stylesheets/lib/normalize.css',
        'public/stylesheets/application.css'
    ];

    var jsFiles = [
        'public/javascripts/lib/jquery-1.10.2.min.js',
        'public/javascripts/lib/twitter-fetcher.js',
        'public/javascripts/application.js'
    ];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        cssmin: {
            options:{
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            combine: {
                files: {
                    'public/stylesheets/opentable.min.css': cssFiles
                }
            }
        },
        uglify: {
            my_target: {
                files: {
                    'public/javascripts/opentable.js': jsFiles
                }
            }
        }

    });

    // Load the plugin that provides the "cssmin" task.
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Configure task(s).
    grunt.registerTask('default', ['cssmin', 'uglify']);
    grunt.registerTask('buildcss', ['cssmin']);
    grunt.registerTask('buildjs', ['uglify']);

};