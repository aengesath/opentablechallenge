(function(){

    google.load("feeds", "1");

    var OpenTable = {

        initialize: function(){

            this.loadTwitterFeed();
            this.loadNewsFeed();

        },

        loadTwitterFeed: function(){

            // Load Twitter Feed
            twitterFetcher.fetch('545610689508036610', function(tweets){
                var html = "";
                for (var i = 0; i < 4; i++){
                    html += "<div class='cell tweet'>";
                    html += "<h5><strong>OpenTable</strong> @OpenTable</h5>";

                    if (tweets[i].innerText) {
                        html += '<p>' + tweets[i].innerText + '</p>';
                    } else {
                        html += '<p>' + tweets[i].textContent + '</p>';
                    }
                    html += "</div>";
                }
                $('#recent-tweets').append(html);

            });

        },

        loadNewsFeed: function(){
            // Load News Feed
            function loadNewsFeed() {
                var feed = new google.feeds.Feed("http://apps.shareholder.com/rss/rss.aspx?channels=7247&companyid=ABEA-4SW9EX&sh_auth=3740540308%2E0%2E0%2E41993%2E1ac47a99456adb91cea015ed95fc7883");
                feed.load(function(result) {
                    if (!result.error) {
                        var max_entries = result.feed.entries.length > 2 ? 2 : result.feed.entries.length;
                        var html = "";
                        for (var i = 0; i < max_entries; i++) {
                            var entry = result.feed.entries[i];
                            html += "<div class='cell news'>";
                            html += "<a href='" + entry.link + "'>" + entry.title + "</a>";
                            html += "<p>" + entry.publishedDate + "</p>";
                            html += "</div>";
                        }
                        $('#recent-news').append(html);
                    }
                });
            }
            google.setOnLoadCallback(loadNewsFeed);
        }
    };

    $(document).ready(function(){
        OpenTable.initialize();
    });

})();